FROM python:3.7-alpine

# Metadata
LABEL mantainer="Gius. Camerlingo <gcamerli@gmail.com>"
LABEL version="1.0"
LABEL description="Image processing server for Docker."

# Docker image name
ENV NAME=img

# Timezone
ENV TZ="Europe/Paris"

# Update system and install packages
RUN apk add --no-cache \
    build-base \
    zlib-dev \
    jpeg-dev \
    iputils \
    freetype-dev

# Volume
VOLUME ["/usr/local/app"]
WORKDIR /usr/local/app

# Pip requirements
COPY requirements .
RUN pip install -r requirements && \
    rm -rf /root/.cache && \
    apk del build-base

# Healthcheck
COPY healthcheck /usr/local/bin/
RUN chmod 744 /usr/local/bin/healthcheck

# Port
EXPOSE 8888

# Entrypoint
ENTRYPOINT [ "/usr/local/app/entrypoint.sh" ]
CMD ["start"]
