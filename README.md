# **Img**

Image processing server in python for spixel.

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/img/src/master/LICENSE.md)**.
